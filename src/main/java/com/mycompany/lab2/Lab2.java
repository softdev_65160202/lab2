package com.mycompany.lab2;

import java.util.Scanner;

public class Lab2 {

    static char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char player = 'X';
    static int row;
    static int col;
    static int count;

    static void printWelcome() {
        System.out.println("Welcome to OX");
    }

    static void printTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }
    }

    static void printTurn() {
        System.out.println("Turn " + player);
    }

    static void printInput() {
        Scanner kb = new Scanner(System.in);
        while(true){
            System.out.print("Please input row, col : ");
            row = kb.nextInt();
            col = kb.nextInt();
            if (table[row - 1][col - 1] == ('-')) {
                table[row - 1][col - 1] = player;
                break;
            } 
        }
    }

    static void switchPlayer() {
        if (player == 'X') {
            player = 'O';
        } else {
            player = 'X';
        }
    }
    
    static boolean isWin() {
        if(checkRow()) {
            return true;
        }if(checkCol()) {
            return true;
        }if(checkX()) {
            return true;
        }if(checkX2()) {
            return true;
        }
        return false;
    }
    
    static boolean isDraw() {
        if(isWin() == false) {
            count++;
            if(count != 9) {
                return false;
            }
        }return true;
    }
    
    static boolean checkRow() {
        for(int i=0; i<3; i++) {
            if(table[row-1][i] != player){
                return false;
            }
        }return true;
    }
    
    static boolean checkCol() {
        for(int i=0; i<3; i++) {
            if(table[i][col-1] != player) {
                return false;
            }
        }return true;
    }
    
    static boolean checkX() {
        for(int i=0; i<3; i++) {
            if(table[i][i] != player) {
                return false;
            }
        }return true;
    }
    
    static boolean checkX2() {        
        if(table[0][2] != player || table[1][1] != player || table[2][0] != player) {
            return false;
        }return true;
    }
    
    static void printWin() {
        System.out.println(player + " win!!");
    }
    
    static void printGameover() {
        System.out.println("Game Over!!");
    }
    
    static void printContinue() {
        System.out.print("Continue? [y/n] : ");
    }
    
    static void printDraw() {
        System.out.println("Draw!!");
    }
    
    
    static boolean checkAns() {
        Scanner kb = new Scanner(System.in);
        String ans = kb.next();
        if(ans.equals("y")) {
            return true;
        }else{
            return false;
        }
    }
    
    static void resetCount() {
        if(count != 0) {
            count = 0;   
        }
    }
    
    static void resetPlayer() {
        player = 'X';
    }
    
    static void newTable() {
        for(int i=0; i<3; i++){
            for(int j=0; j<3; j++) {
                table[i][j] = '-';
            }
        }
    }
    
    public static void main(String[] args) {
        printWelcome();
        while (true) {
            printTable();
            printTurn();
            printInput();
            if(isWin()) {
                printTable();
                printWin();
                printGameover();
                printContinue();
                if(checkAns()) {
                    newTable();
                    resetCount();
                    resetPlayer(); //for new game
                    continue;
                }else{
                    break;
                }
            }if(isDraw()) {
                printDraw();
                printGameover();
                printContinue();
                if(checkAns()) {
                    newTable();
                    resetPlayer();
                    continue;
                }else {
                    break;
                }
            } 
            switchPlayer();
        }

    }
}
